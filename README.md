Static Files for LilyPond
=========================

This project hosts static files for the LilyPond project.
Right now, the following directories are served from the directory [`public`](public/):
 * [`media`](public/media/): Content linked from the website at https://lilypond.org
